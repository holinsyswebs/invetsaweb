<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_invetsa');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-9(nX;=Ryt6]Tyui>;9$AR#yCT{j+Z*sW<+s(rQoT5P<MPyVh/jyQl{CsHeA#r!6');
define('SECURE_AUTH_KEY',  '?0[}tn}6& |Gto%Zdz[,exq7_=f5k.3u;.#q?VZn*vYb>0wv(|_5sNDR&PNM4X^Q');
define('LOGGED_IN_KEY',    '!v<)f-`%vruTU;dQy:SD2!1lE#Q [% UP`A^~n|0ay032]$} FX&;,CkLK@KJEEf');
define('NONCE_KEY',        '[8A|GLKp2O*(nYTb+6@j#ks+&|(Z^mH:TOyPog*q 8mPX(iAju0rX.T/:)D6Y-Zl');
define('AUTH_SALT',        'Ui`xr)9$BPv+?*gI,u:frI6a (C)JGfSd|9,z`T+ovMU4Fv]1n*_xpr7EyGn^t63');
define('SECURE_AUTH_SALT', 'Wk$kzRK_;%TM@a||i?Nnl]pBFM8XrxS9]MIl15mP/DA3n ^gP-B9ckaI7?U#ylKa');
define('LOGGED_IN_SALT',   '3wB$:gXp&`&JO7IeLkS!}SZqPy.wJ^8F}R|ON:.?Ymd=JL}1@i?t6Q{ ?(_9h3]N');
define('NONCE_SALT',       'B%l9)F;.TtUtIUzpA,@f?}]C*T/2pOXXkxU^*6myo=>_5RmNo7MdEq3G+2R]KEop');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'his_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
